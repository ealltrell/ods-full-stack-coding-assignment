import React, {useState, useEffect} from 'react';

import Select from 'react-select';

import ResultsTable from './components/ResultsTable';

function App() {
  const [stationNames, setStationNames] = useState([]);
  const [results, setResults] = useState(null);

  useEffect(() => {
    if (!stationNames.length) {
      fetchStationNames();
    }
  })

  const fetchStationNames = (query) => {
    fetch(`http://localhost:5000/api/v1/search_names${query?.length > 0 ? '?query=' + query : ''}`)
      .then(res => res.json())
      .then(
        (result) => {
          setStationNames(result.map(item => {
            return {
              value: item[0],
              label: `${item[0]} - ${item[1]}`
            }
          }));
        },
        (error) => {
          console.log('Error:', error)
        }
      )
  }

  const searchFlights = (station = '', action) => {
    if (action === 'clear') {
      setResults(null);
    } else {
      fetch(`http://localhost:5000/api/v1/flights?station=${station.value}`)
        .then(res => res.json())
        .then(
          (result) => {
            setResults([result[0], ...result[1]]);
          },
          (error) => {
            console.log('Error:', error)
          }
        )
    }
  }

  return (
    <div className="container">
      <div className="my-5">
        <Select
          className="mb-3"
          options={stationNames}
          isClearable
          onInputChange={(val) => {
            fetchStationNames(val);
          }}
          onChange={(selectedStation, type) => searchFlights(selectedStation, type.action)}
        />
      </div>
      {results && <ResultsTable results={results.slice(1)} headers={results[0]}/>}
    </div>
  );
}

export default App;
