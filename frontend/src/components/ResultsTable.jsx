export default (props) => {
  const {results, headers} = props;

  return (
    <table className="table table-striped border table-responsive table-hover position-relative">
      <thead>
        <tr>
          {results.length > 0 && headers.map((header, idx) => {
            return <th key={idx} scope="col" className="mx-3" style={{position: 'sticky', top: 0}}>{header}</th>
          })}
        </tr>
      </thead>
      <tbody>
        {results.map((result, idx) => {
          return (
            <tr key={idx}>
              {headers.map((header, idx2) => {
                return <th key={idx2}>{result[header]}</th>
              })}
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}