from flask import Flask, jsonify, request, redirect, url_for
from flask_cors import CORS
import csv

app = Flask(__name__)
CORS(app)

first_line = True
flightsArr = []
headers = []
station_names = {}
with open('../data/flights.csv') as csv_file:
  data = csv.reader(csv_file, delimiter=',')
  for row in data:
    if first_line:
      headers = row
      first_line = False
    else:
      if row[11] not in station_names:
        station_names[row[11]] = ((row[11], row[13]))
      if row[12] not in station_names:
        station_names[row[12]] = ((row[12], row[14]))

      flightsArr.append({
        'id': row[0],
        'created_at': row[1],
        'updated_at': row[2],
        'flight_identifier': row[3],
        'flt_num': row[4],
        'scheduled_origin_gate': row[5],
        'scheduled_destination_gate': row[6],
        'out_gmt': row[7],
        'in_gmt': row[8],
        'off_gmt': row[9],
        'on_gmt': row[10],
        'destination': row[11],
        'origin': row[12],
        'destination_full_name': row[13],
        'origin_full_name': row[14]
      })

@app.route("/api/v1/flights", methods=["GET"])
def flights():
  station = request.args.get('station')
  filter_col = request.args.get('filter_col')
  returnArr = [headers]
  if station != None:
    if filter_col != None:
      returnArr.append([flight for flight in flightsArr if flight[filter_col] == station])
      return jsonify(returnArr), 200
    returnArr.append([flight for flight in flightsArr if flight['origin'] == station or flight['destination'] == station])
    return jsonify(returnArr), 200
  returnArr.append(flightsArr)
  return jsonify(returnArr), 200

@app.route("/api/v1/search_names", methods=["GET"])
def search_names():
  search_query = request.args.get('query')
  if search_query != None and len(search_query):
    filtered_names = []
    for code in station_names:
      if search_query.lower() in station_names[code][0].lower() or search_query.lower() in station_names[code][1].lower():
        filtered_names.append((code, station_names[code][1]))
    return jsonify(filtered_names), 200
  return jsonify(list(station_names.values())), 200

if __name__ == "__main__":
  app.run()