# Submission
## To run the project:

* In order to run this project you'll need `npm` and `python` installed locally.

From the root directory, cd into `backend` and start the pipenv shell with the command `pipenv shell`, then run `python api.py` to start listening on http://localhost:5000 for calls to the available api endpoints.

To start the frontend, simply cd into the `frontend` folder, run `npm i` and then `npm start` and the project will be running on http://localhost:3000.

## Project description:
The backend is very basic, just two endpoints: `/api/v1/flights` and `/api/v1/search_name`

* The `flights` endpoint checks for two optional query parameters, `station` and `filter_col`. `station` allows the caller to specify specific stations in either flight destinations or origins, and `filter_col` allows to designate either flight destinations or origins specifically. Providing no query parameters will return all flights as an array/list. The first element of the returned array contains all the headers used for displaying a table of the results.
* The `search_name` endpoint checks for a single query parameter, `query` which will be checked against the full names of the available stations. Any matches found will be returned in an array/list.

The frontend is even simpler, just an input field allowing the user to search for specific stations and a table to display the results. 

## Next steps:
For the frontend, clicking on table elements could show a more compact view of the provided data since it does take up more width than an average computer screen. The table could be filterable by the provided headers.

For the backend, endpoints could be made to allow for more search/filter options. Flights could be searched by date created/updated (even though those values are all the same in the provided data), or a specific date range. You could search by flight number as well as by origin/departure gate. You could also allow for searching for more than one station as only one is allowed now. 
